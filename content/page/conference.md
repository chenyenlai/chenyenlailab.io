---
title: Conference
description: Attending record
date: 2017-01-01
menu: main
weight: -90
comments: false
---
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

#### Future Conference

- APS March Meeting, Denver CO, 2020

#### Talks (Bold face are invited)

-   *“Ultrafast spectroscopy of strongly correlated systems.”* (Poster)  
    Strongly Correlated Quantum Materials, Santa Fe NM, Apr 29, 2019.

-   *“Non-equilibrium dynamics of spin and charge correlation in strongly correlated systems from pump-probe spectroscopy."*  
    APS March Meeting, Boston MA, Mar 6, 2019.

-   *“Microscopic dynamics in the Holstein model at high temperature: Absence of diffusion."*  
    APS March Meeting, Boston MA, Mar 6, 2019.

-   *“Core hole effects of the time-resolved x-ray absorption spectroscopy in a strongly correlated electron system."*  
    **LANL Quantum Day**, Los Alamos National Laboratory, Dec 11, 2018.

-   *“Measuring and quantifying quantum memory effects in cold atom systems."*  
    **APS Four Corners Section**, Salt Lake City, UT, Oct 13, 2018.

-   *“Dynamics of Hostein model in strong coupling and high temperature: charge pumping and localization.”*  
    **BLABS, Theoretical Division, Los Alamos National Laboratory**, May 21, 2018.

-   *“Time-resolved X-ray Spectroscopy in One-dimensional Strongly Correlated Systems.”*  
    APS March Meeting, Los Angeles CA, 2018.

-   *“Quantum memory effects through interaction imbalance in ultracold bosonic and fermonic atoms.”*  
    DAMOP 48th Annual Meeting, Sacramento CA, 2017.

-   *“Close and open system approaches to ultracold Atoms: Implications For Atomtronics.”*  
    **Los Alamos Nation Laboratory - Theoretical Division (T-4)**, Mar 27, 2017.

-   *“Quantum memory effects in ultracold quantum gases.”*  
    **Karlsruhe Institute of Technology - Campus Süd, Germany**, Mar 9, 2017.

-   *“Close and open system approaches to ultracold Atoms: Implications For Atomtronics.”*  
    **Karlsruhe Institute of Technology - Campus Nord, Germany**, Mar 8, 2017.

-   *“Non-equilibrium bosonic transport through local manipulations in closed and open quantum systems.”*  
    APS March Meeting, New Orleans LA, 2017.

-   *“Bosonic transport through local manipulations in closed and open quantum systems.”* (Poster)  
    California Institute of Quantum Emulation (CAIQuE) Collaboration Meeting, UCLA, 2016.

-   *"Memory effects in noninteracting isolated systems from dynamical geometry transformations in ultracold quantum gases."*  
    APS March Meeting, Baltimore MD, 2016.

-   *“Source, Sink, and Their Dynamics in Cold-Atom System.”* (Poster)  
    12th US-Japan Seminar, Madison WI, September 2015.

-   *“Non-equilibrium dynamics of atomic Fermi and Bose gas under lattice geometry transformation.”*  
    APS March Meeting, San Antonio TX, 2015.

-   *“\\(d_{xy}\\)-density wave in fermion-fermion cold atom mixtures.”*  
    **University of California, Merced: Physics Colloquium**, Sep 5, 2014.

-   *“\\(d_{xy}\\)-density wave in fermion-fermion cold atom mixtures.”*  
    APS March Meeting, Denver CO, 2014.

-   *“Dipolar Fermions in Quasi-Two-Dimensional Square Lattice.”*  
    APS March Meeting, Baltimore MD, 2013.

-   *“Instabilities of Spin-polarized Fermions in Optical Lattice.”*  
    DAMOP 43th Annual Meeting, Anaheim CA, 2012.

-   *“Pairing and Density-Wave Phases of Population Imbalanced Fermi-Fermi Mixture on Optical Lattice.”*  
    APS March Meeting, Boston MA, 2012.

-   *“Functional Renormalization Group Analysis of Population Imbalance Fermi-Fermi Mixture in Optical Lattice”*  
    **National Tsing-Hua University**, Jun 23, 2011.

-   *“Single Spin Decoherence by General Spin Chains.”*  
    APS March Meeting, Denver CO, 2007.
