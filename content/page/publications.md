---
title: Publications
description: All well ends well
date: 2017-01-01
menu: main
weight: -100
comments: false
---
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

#### WIP

2   **Chen-Yen Lai**, S. A. Trugman, D. A. Yarotski, and Jian-Xin Zhu
    *“Time-resolved Optical Absorption Spectroscopy in a Terahertz-driven Hybrid System of Plasmons and Excitons.”*  
    In preparation.

1   **Chen-Yen Lai** and S. A. Trugman,  
    *“Absence of diffusion and fractal geometry in Holstein model at high temperature.”*  
    In preparation.

#### Preprint

#### Peer-reviewed

18   **Chen-Yen Lai** and Jian-Xin Zhu,  
    *“Ultrafast X-ray Absorption Spectroscopy of Strongly Sorrelated Systems: Core Hole Effect.”*  
    [Phys. Rev. Lett. **122**, 207401 (2019)](https://journals.aps.org/prl/accepted/2d072Y60Jbd1215cf46133d131169f6302c44946d)

17  **Chen-Yen Lai**, S. A. Trugman and Jian-Xin Zhu,  
    *“Optical absorption spectroscopy of plasmon exciton hybrid systems.”*  
    [Nanoscale **11**, 2037-2047 (2019)](https://pubs.rsc.org/en/content/articlelanding/2019/NR/C8NR02310G#!divAbstract).

16  Mekena Metcalf, **Chen-Yen Lai**, Massimiliano Di Ventra and Chih-Chun Chien,  
    *“Many-body multi-valuedness of particle-current variance in closed and open cold-atom systems.”*  
    [Phys. Rev. A **98**, 053601 (2018)](https://journals.aps.org/pra/abstract/10.1103/PhysRevA.98.053601)

15  **Chen-Yen Lai**, Massimiliano Di Ventra, Michael Scheibner and Chih-Chun Chien,  
    *“Tunable current circulation in triangular quantum-dot metastructures.”*  
     [EPL (Europhysics Letters) **123**, 47002(2018)](http://iopscience.iop.org/article/10.1209/0295-5075/123/47002)

14  **Chen-Yen Lai**, and Chih-Chun Chien,  
    *“Quantification of steady-state memory effects from interaction-induced transport in quantum systems.”*  
    [Phys. Rev. A **96**, 033628, (2017)](https://journals.aps.org/pra/abstract/10.1103/PhysRevA.96.033628)

13  Mekena Metcalf, **Chen-Yen Lai**, and Chih-Chun Chien,  
    *“Protocols for dynamically probing topological edge states and dimerization with atomic fermions in optical potentials.”*  
    [EPL (Europhysics Letters) **118**, 56004 (2017)](http://iopscience.iop.org/article/10.1209/0295-5075/118/56004)

12  **Chen-Yen Lai** and Chih-Chun Chien,  
    *“Challenges and constraints of dynamically emerged source and sink in atomtronic circuits: From closed-system to open-system approaches.”*  
    [Sci. Rep. **6**, 37256 (2016)](http://www.nature.com/articles/srep37256)

11  Mekena Metcalf, **Chen-Yen Lai** and Chih-Chun Chien,  
    *“Hysteresis of noninteracting and spin-orbit-coupled atomic Fermi gases with relaxation.”*  
    [Phys. Rev. A **93**, 053617 (2016)](http://journals.aps.org/pra/abstract/10.1103/PhysRevA.93.053617)

10  **Chen-Yen Lai** and Chih-Chun Chien,  
    *“Geometry-Induced Memory Effects in Isolated Quantum Systems: Cold-Atom Applications.”*  
    [Phys. Rev. Applied **5**, 034001 (2016)](http://journals.aps.org/prapplied/abstract/10.1103/PhysRevApplied.5.034001)

9   Xiu-Hao Deng, **Chen-Yen Lai** and Chih-Chun Chien,  
    *“Superconducting circuit simulator of Bose-Hubbard model with a flat band: Quantum phases and their signatures.”*  
    [Phys. Rev. B **93**, 054116 (2016)](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.93.054116)

8   Haiyuan Zou, Yuzhi Liu, **Chen-Yen Lai**, J. Unmuth-Yockey, Li-Ping Yang, A. Bazavov, Z.Y. Xie, T. Xiang, S. Chandrasekharan, Shan-Wen Tsai, and Y. Meurice,  
    *“Progress towards quantum simulating the classical \\(\text{O}(2)\\) model.”*  
    [Phys. Rev. A **90**, 063603 (2014)](http://journals.aps.org/pra/abstract/10.1103/PhysRevA.90.063603)

7   **Chen-Yen Lai**, Wen-Min Huang, David K. Campbell, and Shan-Wen Tsai,  
    *“\\(d_{xy}\\)-density wave in fermion-fermion cold atom mixtures.”*  
    [Phys. Rev. A **90**, 013610 (2014)](http://journals.aps.org/pra/abstract/10.1103/PhysRevA.90.013610)

6   Wen-Min Huang, **Chen-Yen Lai**, Chuntai Shi, Shan-Wen Tsai,  
    *“Unconventional superconducting phases on a two-dimensional extended Hubbard model.”*  
    [Phys. Rev. B **88**, 054504 (2013)](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.88.054504).

5   **Chen-Yen Lai**, Chuntai Shi, Shan-Wen Tsai,  
    *“Correlated phases of population imbalanced Fermi-Fermi mixtures on an optical lattice.”*  
    [Phys. Rev. B **87**, 075134 (2013)](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.87.075134).

4   Pochung Chen, **Chen-Yen Lai**, Min-Fong Yang,  
    *“Field Induced Spin Supersolidity in Frustrated Spin-1/2 Spin-Dimer Models.”*  
    [Phys. Rev. B **81**, 020409(R) (2010)](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.81.020409)

3   Pochung Chen, **Chen-Yen Lai**, and Min-Fang Yang,  
    *“Numerical Study of Spin-1/2 \\(XXZ\\) Model on Square Lattice from Tensor Product States.”*  
    [J. Stat. Mech.: Theory Exp. (2009) P10001](http://iopscience.iop.org/1742-5468/2009/10/P10001/)

2   Chi-Ming Chang, Wei-Chao Shen, **Chen-Yen Lai**, Pochung Chen, and Daw-Wei Wang,  
    *“Spontaneous inter-wire coherence of polar molecules in double-wire systems.”*  
    [Phys. Rev. A **79**, 053630 (2009)](http://journals.aps.org/pra/abstract/10.1103/PhysRevA.79.053630)

1   **Cheng-Yan Lai**, Jo-Tzu Hung, Chung-Yu Mou and Pochung Chen,  
    *“Induced decoherence and entanglement by interacting quantum spin baths.”*  
    [Phys. Rev. B **77**, 205419 (2008)](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.77.205419)

#### Conference Proceedings

2   Alexei Bazavov, **Chen-Yen Lai**, Yannick Meurice, Shan-Wen Tsai,  
    *“The effective U(1)-Higgs theory at strong coupling on optical lattices?”*  
    32nd International Symposium on Lattice Field Theory, Columbia University, New York City, NY, June 23-28, 2014.

1   Pochung Chen, **Cheng-Yan Lai**, Jo-Tzu Hung, and Chung-Yu Mou,  
    *“Quantum Spin Baths Induced Transition of Decoherence and Entanglement”*  
    AIP Conf. Proc. 1074, 72-78 (2008) [doi:10.1063 / 1.3037141](http://dx.doi.org/10.1063/1.3037141),  
    2nd International Workshop on Solid-State Quantum Computing/Mini-School on Quantum Information Science, Taipei, TAIWAN, June 23-27, 2008.

#### Other publications

-   Chen-Yen Lai, **Spin Qubit Decoherence by Spin Bath – a Time-Dependent DMRG Study**,
    [Master Thesis](http://etd.lib.nctu.edu.tw/cgi-bin/gs32/hugsweb.cgi?o=dnthucdr&s=id=%22GH000943338%22.&searchmode=basic), July 2007.

-   Chen-Yen Lai, **Unconventional Density Wave and Superfluidity in Cold Atom Systems**,
    [Ph.D. Thesis](http://www.etdadmin.com/cgi-bin/student/etd?siteId=165;submissionId=277352), June 2014.
