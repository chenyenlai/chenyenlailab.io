---
title: CV
description: Academic record
date: 2017-01-01
menu: main
weight: -110
comments: false
---

#### Research Appointments

* **(2017-)** *Postdoctoral Associate*  
  Theoretical division (T-4) and Center for integrated nanotechnologies  
  Los Alamos National Laboratory, Los Alamos NM 87545 USA

* **(2014-2017)** *Postdoctoral Scholar*  
  School of Natural Science  
  University of California, Merced, Merced CA 95340 USA

* **(2008-2009)** *Research Assistance*  
  Department of Physics  
  National Tsing Hua University, Hsinchu 30013 Taiwan

#### Educations

* **(2009-2014)** *Doctor of Philosophy*  
  Department of Physics  
  University of California, Riverside, Riverside CA 92521 USA

* **(2005-2007)** *Master of Science*  
  Department of Physics  
  National Tsing Hua University, Hsinchu 30013 Taiwan

* **(2001-2005)** *Bachelor of Science*  
  Department of Physics  
  National Sun Yat-sen University, Kaohsiung 80424 Taiwan

#### Awards

* *Institutional Computing Program Allocation Award 2018 - 6M Service Hours*  
  Los Alamos National Laboratory, Los Alamos NM 87545 USA
 
* *XSEDE High Performance Computing Allocation Award 2014*

* *Robert T. Poe Memorial Scholarship Award for Outstanding PhD Graduate*  
  Department of Physics  
  University of California, Riverside, Riverside CA 92521 USA

* *Outstanding Teaching Assistant for academic year 2011*  
  Department of Physics  
  University of California, Riverside, Riverside CA 92521 USA

* *Annual Student Journal Paper Award*  
  Department of Physics  
  National Tsing Hua University, Hsinchu 30013 Taiwan

#### Projects

* Active developer of [Uni10](https://uni10.gitlab.io/), the Universal Tensor Network Library, is geared toward applications to the tensor network algorithms, such DMRG, TEBD, PEPS and MERA.
