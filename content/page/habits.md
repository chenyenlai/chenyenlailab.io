---
title: MISC
description: Some of my habits
date: 2017-01-01
menu: main
weight: -70
comments: false
---

##### Photography
One of my long term habits since my childhood. 
Most of my work are on natural scene and wild animals. 
If you are interested, please pay a visit to [my flickr page](https://www.flickr.com/photos/xavierweathertoplai/)

##### Hiking

##### Downhill Skiing
I learned how to ski on 2016-17 winter with my son (he was 4 years old) at [Ski China Peak](http://www.skichinapeak.com).
I admit that I am a slow learner on this
(Yes, I fall all the time.)
I moved to Los Alamos on summer 2017, and the 2017-18 winter is bad!
There is only one beginner trail opened in [Pajarito Mountain](https://www.pajarito.ski).
Even with this easiest slope, I still crash all the time and I injured my knee which takes couple months to heal.
The 2018-19 winter is epic, and my wife joins the skiing group.
I got my skill advanced this year from practicing and practicing
(Yes, there is no other way.)
I learned most of the technique and drills from YouTube, including parallel turns, skidding turns, carving turns, bumps, jumps and some simple tricks.
Now, I can ski black lines, trees, and moguls.
I have ski in several ski resorts, including  
1. [China Peak](http://www.skichinapeak.com),  
2. [Dodge Ridge](https://www.dodgeridge.com),  
3. [Sipapu](https://www.sipapu.ski),  
4. [Pajarito](https://www.pajarito.ski),  
5. [Wolf Creek](https://wolfcreekski.com).  
