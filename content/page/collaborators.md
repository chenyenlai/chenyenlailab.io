---
title: Collaborators
description: Past and present
date: 2017-01-01
menu: main
weight: -80
comments: false
---

##### Dr. Jian-Xin Zhu  
Los Alamos National Laboratory

##### Dr. Stuart A. Trugman  
Los Alamos National Laboratory

##### Dr. Jhih-Shih You  
The Leibniz Institute for Solid State and Materials Research

##### Prof. [Chih-Chun Chien](https://sites.google.com/site/chienchihchun/)  
University of California, Merced

##### Dr. [Mekena Metcalf](https://rodeophysicist.com/)
Lawrence Berkeley National Laboratory

##### Prof. [Massimiliano Di Ventra](https://diventra.physics.ucsd.edu/)  
University of California, San Diego

##### Prof. [Michael Scheibner](http://faculty.ucmerced.edu/mscheibner/)  
University of California, Merced

##### Prof. Shan-Wen Tsai  
University of California, Riverside

##### Prof. [Yannick Meurice](http://www-hep.physics.uiowa.edu/~meurice/)  
University of Iowa

##### Prof. [Pochung Chen](http://qubit.phys.nthu.edu.tw)
National Tsing Hua University
