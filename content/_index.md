## Fun never ends ......

I am currently a postdoctoral researcher working on condensed matter theory at Los Alamos National Laboratory.
In 2014, I was honored Ph.D. with Robert T. Poe Memorial Scholarship Award for Outstanding PhD Graduate in University of California, Riverside.
My Ph.D. thesis focues on the Fermi liquid instability in cold atom systems, especially systems hard to found in materials, for example, spin-imbalanced system and two component Fermi gas system.
In the following three years, I join Univeristy of California, Merced as a postdoctoral scholar and working on non-eqilibrium phenomena in cold atom systems, including quench dynamics and quantum memory effect.
We discovered that the quantum memory effect can be measured and quantified from either tuning geometry involved a flat band or through strongly correlated effects.
In summer 2017, I moved to Los Alamos and work at the center of integrated nanotechnologies as a Postdoc.
I am currently working on the non-equilibrium dynamics which involved different degrees of freedom.
For instance, optical absorption spctrum in hybrid plasmon-exciton system, time-resolved spectroscopy in strongly correlated systems, and dynamics in Holstein model.

My research interest includes, but not limited to, Fermi liquid instability, quantum phase transition, non-equilibrium phenomenon, and spectroscopy in both condensed matter and cold atom systems.
Also, renormalization group and computational physics are also on my focus. 

#### Tensor Network States
The state-of-the-art algorithm called density matrix renormalization group (DMRG) can provide the numerical exact solution of strongly correlated systems in low dimension.
The finite temperature and time evolution can also be implemented.
In higher dimension, projected entangle pair state (PEPS) is a great candidate for looking quantum phases.
I am also currently active developer of [uni10](https://uni10.gitlab.io/) library.

Most of my publications can be found in [arXiv](https://arxiv.org/a/lai_c_1.html).
Followings are my recent works in condensed matter theory.
