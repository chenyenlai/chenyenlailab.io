---
title: Variance of a quantum observable
subtitle: Basic quantum mechanics?
date: 2018-05-01
tags: ["Transport", "OQS"]
---

[arXiv:1805.06443](https://arxiv.org/abs/1805.06443) (LA-UR-18-23857)  
As we learned the basic notion of quantum mechanics, uncertainty principle, the measurement in quantum theory results in probability.
As people usually discuss the average of an observable, its variance, however, is not discussed in the scope.
We discuss a basic quantity in quantum transport - variance of the current.
Two systems are discussed, 1) closed system with magnetic flux which is also known as persistent current; 2) open quantum system of simple connections.
Both can be realized in cold atom systems and quantitatively measure the effects discussed in the article.
The major finding is that the correlated effect (a.k.a interactions between fermions) causes the multi-valuedness of variance of the current under constant steady state current.
We plot the Lissajous curves to present this phenomenon as (d) in the figure.

{{< figure src="/media/Fluctuations/oqs.jpg" title="" width="55%" >}}
