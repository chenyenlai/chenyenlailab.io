---
title: Current circulation in triangle quantum-dot metastructures
subtitle: Novel transport phenomenon
date: 2018-02-06
tags: ["Transport", "OQS"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}] 
---

[arXiv:1802.02121](https://arxiv.org/abs/1802.02121) (LA-UR-18-20615)  
In this work, we study transport in a simple triangle quantum-dot metastructures.
By modeling the systems as open quantum systems, ***circulating current is found without external magnetic field/vector potential***.
The same finding is also discovered in isolated systems.
The models and results can be tested in proposed experiments.
This study reveals interesting transport phenomenon in non-trivial topology and geometry of the quantum systems.

{{< figure src="/media/Circulation/f1.png" title="Proposed Experiments" width="55%" >}}
{{< figure src="/media/Circulation/f2.png" title="Current Circulation" width="55%" >}}
