---
title: Time-resolved x-ray absorption spectroscopy
subtitle: Pump-probe
date: 2018-08-28
tags: ["Ultrafast", "Spectrum"]
---

[arXiv:1808.08243](https://arxiv.org/abs/1808.08243) (LA-UR-17-29749)  
We investigate the ultrafast pump-probe phenomenon in strongly correlated systems.
The static x-ray absorption spectroscopy (XAS) reveals the metal-insulator transition and strongly correlated effects in one-dimensional Fermi Hubbard model.
The laser pump excites the system into a thermal state where the excitations can be measured from the probe pulse.
The resonance between the frequency of the pump pulse and the Mott gap can be observed from both the frequency shift and the change of weight.
In addition, the driven Mott insulating state has a metal droplet state near the core hole created by the x-ray.
The influence from different pulse shape, width, and frequency are studied and discussed.
